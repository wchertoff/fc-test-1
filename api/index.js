const micro = require('micro')
const mysql = require('mysql2')

// Config ⚙️
const DB_CONFIG = {
  host: process.env.DB_HOST || '127.0.0.1',
  database: process.env.DB_NAME || 'funding_circle_test',
  user: process.env.DB_USER || 'root',
  password: process.env.DB_PASSWORD || ''
}

// Queries 🔍
const HELLO_NAME = 'SELECT name from Names LIMIT 1'

// DB Query 🕵🏻‍♂️
async function query(db, queryString) {
  let result
  try {
    const [rows, fields] = await db.execute(queryString)
    result = [rows, null]
  } catch (error) {
    result = [null, error]
  }
  return result
}

module.exports = async (req, res) => {
  const connection = await mysql.createConnectionPromise(DB_CONFIG)
  try {
    const [data, queryError] = await query(connection, HELLO_NAME)
    if (queryError) {
      connection.close()
      return `Query Error: ${queryError}`
    }
    connection.close()
    return data
  } catch (connectionError) {
    return `Connection Error: ${connectionError}`
  }
  return 'No Result'
}
