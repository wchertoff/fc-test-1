# On-Site-Interview

## Interviews

### Network Security

#### Alberto Martinez

#### My Experience

* I have experience setting up security groups to restrict Backend EC2 instances to only interacting with Application Instances

##### Questions

```
- What tools does Funding Circle use to ensure Security?
- How do you manage User Permissions at a large scale?
- Can you describe Penetration Testing methods employed here?
- Can you tell me about the tools you use for Logging Security Processes?
```

### Junior DevOps Engineer

#### Gabriel Vega

##### My Experience

* I have experience with multiple Application stacks as well as DevOps stacks. Currently, I am working mostly within the modern Javascript Ecosystem for application code and AWS and Docker for Ops
* I have deployed multiple applications on EC2 using Docker. These applications don't receive high levels of traffic, so no automatic load balancing has been implemented.
* I have used google cloud storage buckets to serve Single Page application code for integration with monolithic CMS sites.

##### Questions

```
- What steps did you take to get up to speed with the Funding Circle Architecture?
- Walk me through a typical day.
- What are some of the current challenges that the Funding Circle Architecture faces?
- What platforms besides AWS are used?
- Where do you feel like the most help or support is currently needed on the team?
```

### Senior Software Engineer

#### Molly Huerster

#### My Experience

* I have experience with multiple Application Stacks including PHP, Rails and Javascript. Currently, I mostly working within the modern Javascript Ecosystem for Application code, and AWS EC2 for Application code containerized deployment.
* I have written UIs mostly in React, API's in node.js and serverless functions in Javascript.

##### Questions

```
- Could you tell me about what sort of systems are in place to coordinate DevOps and Software Devs?
- How do you handle CI/CD?
- How does the dev team submit new architecture requests?
- How does the promotion process work, or how does a developer become a senior developer?
```

### Senior Infrastructure Engineer

#### Igor Boshoer

#### My Experience

* I have experience with multiple Application stacks as well as DevOps stacks. Currently, I am working mostly within the modern Javascript Ecosystem for application code and AWS and Docker for Ops
* I have deployed multiple applications on EC2 using Docker. These applications don't receive high levels of traffic, so no automatic load balancing has been implemented.
* I have used google cloud storage buckets to serve Single Page application code for integration with monalithic CMS sites.

##### Questions

```
- Can you discuss a bit about how the infrastructure has changed since you've been here, and how the team handled those changes?
- Could you walk me through the process in which the team decides on an architecture improvement, and then implement that improvement?
```

### Infrastructure Manager

#### Kevin O'Shaughnessy

```
- How has the team delt with disaster relief in the past? (if they've had to)
- What's 2018 look like for Funding Circle Dev Ops? What are our current challenges, goals and desired improvements?
```
