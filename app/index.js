const fetch = require('fetch-everywhere')

/* API Link 🗺 */
const API_ADDRESS =
  process.env.API_ADDRESS || 'https://api.github.com/users/willchertoff'

const pStyles = `font-size: 40px;
font-family: monospace;
color: lightcoral;`

const bStyles = `display: flex;
justify-content: center;
height: 100vh;
align-items: center;`

const html = (name = 'unknown') => `
<!DOCTYPE html>
  <html lang="en">
    <body style="${bStyles}">
      <p style="${pStyles}">Hello ${name}, I sure hope you pass &#9996;</p>
    </bod>
  </html>
`

module.exports = async (req, res) => {
  try {
    const response = await fetch(API_ADDRESS)
    const json = await response.json()
    if (json.length > 0) {
      const name = json.shift().name || 'Unknown'
      return html(name)
    } else {
      return 'no length to json'
    }
  } catch (error) {
    return 'There was an error'
  }

  return `Could not Fetch Data :(`
}
