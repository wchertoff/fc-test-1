const AWS = require('aws-sdk')
const { dateTag, today, fortnight } = require('./helpers')

// Setup AWS Config with IAM credentials
AWS.config.update({
  region: 'us-west-2',
  credentials: {
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey
  }
})
// Establish EC2 Service Object
const ec2 = new AWS.EC2()

/**
 * Entry point for Program. Creates new snapshots, deletes expired ones.
 */
const snapShotWorker = () => {
  // Create Snapshots
  ec2.describeInstances({}, (err, data) => {
    if (err) {
      console.log(err)
      return
    }
    data.Reservations.forEach(r => {
      r.Instances.forEach(createSnapshot)
    })
  })

  // Delete Expired Snapshots
  const deleteParams = {
    Filters: [
      { Name: 'tag-key', Values: ['DeleteOn'] },
      { Name: 'tag-value', Values: [today()] }
    ]
  }
  ec2.describeSnapshots(deleteParams, (err, data) => {
    if (err) {
      console.log(err)
    } else {
      data.Snapshots.forEach(s => deleteSnapshot(s))
    }
  })
}

/**
 * Creates a Snapshot
 * @param {EC2.Instance} instance - The instance to create a snapshot for
 */
const createSnapshot = instance => {
  const volumes = getInstanceVolumes(instance)

  volumes.forEach(vol => {
    const params = {
      VolumeId: vol,
      DryRun: false
    }
    const tags = getTags(instance)
    ec2.createSnapshot(params, (err, data) => {
      if (err) {
        console.log(err)
      } else {
        tagInstance(data.SnapshotId, instance, tags)
      }
    })
  })
}

/**
 * Deletes a Snapshot
 * @param {EC2.Snapshot} snapshot - The snapshot to delete
 */
const deleteSnapshot = ({ SnapshotId }) => {
  const params = {
    SnapshotId
  }
  ec2.deleteSnapshot(params, (err, data) => {
    if (err) {
      console.log(err)
    } else {
      console.log(`Deleted Snapshot id ${SnapshotId}`)
    }
  })
}

/**
 * Get Tags from instance
 * @param {EC2.Instance} instance - The instance to get tags from
 */
const getTags = instance =>
  instance.Tags.filter(tag => tag.Key === 'aws:cloudformation:logical-id')

/**
 * Tag EC2 Snapshot
 * @param {Int} snapshotId - The id of snapshot to tag
 * @param {EC2.Instance} instance - The instance associated with the tag
 * @param {Array} tags - Array of Tags for instance
 */
const tagInstance = (snapshotId, instance, tags) => {
  var params = {
    Resources: [snapshotId],
    Tags: [
      {
        Key: 'Name',
        Value: `${tags[0].Value} - ${instance.InstanceId}`
      },
      {
        Key: 'Date',
        Value: dateTag()
      },
      {
        Key: 'DeleteOn',
        Value: fortnight()
      }
    ]
  }

  ec2.createTags(params, (err, data) => {
    if (err) {
      console.log(err)
    } else {
      console.log(`Created Snapshot`)
    }
  })
}

/**
 * Gets Volumes for Instance
 * @param {EC2.Instance} instance - The instance to get Volumes for
 */
const getInstanceVolumes = instance => {
  const volumes = []
  instance.BlockDeviceMappings.forEach(i => {
    volumes.push(i.Ebs.VolumeId)
  })
  return volumes
}

// Run script
snapShotWorker()
