const moment = require('moment')

const today = () =>
  moment()
    .format('MM DD')
    .toString()

const fortnight = () =>
  moment()
    .add('14', 'days')
    .format('MM DD')
    .toString()

const dateTag = () => moment().toString()

module.exports = {
  today,
  fortnight,
  dateTag
}
