# Global DevOps Tech Test 1

## Links 🔗

* [Assignment Link](https://gitlab.com/wchertoff/fc-test-1/blob/master/globaldevops-techassignment.pdf)
* [EC2 App 1](http://ec2-54-202-203-173.us-west-2.compute.amazonaws.com)
* [EC2 App 2](http://ec2-54-202-64-129.us-west-2.compute.amazonaws.com)

## Solutions

### Architecture

Amazon Cloud Formation is used to streamline stack creation.

#### Stack

* 2 Ubuntu linux instances to provide front end Application code
* 1 Ubuntu linux instance to provide API connectivity to the Backend
* 1 MySQL AWS Relational Database to provide Backend data for the stack.

This stack was created via the configuration found in `stack.yml`. The command-tool was chosen for the following reasons:

* Taking a declarative approach to stack creation provides clarity and ease of scale to stack development
* Multiple stacks can be created via the same configuration
* Navigating the AWS web UI can take up a lot of time

#### Setup

Install Command Line Tools
```bash
$ pip install awscli --upgrade --user
```

Configure AWS credentials
```bash
$ aws configure --profile [your profile name]
AWS Access Key ID [None]: [your access key]
AWS Secret Access Key [None]: [your secret access key]
Default region name [None]: [your region]
Default output format [None]: [your output format]
```

Deploy Stack
```bash
aws cloudformation creat-stack --stack-name [your stack name here] --template-body stack.yml --profile [your profile name] --reg [your region]
```

You can monitor the stack creation in the AWS Cloud Formation console.

### Application Code (`/app`)

#### Build

Both the **API** and **App** are deployed in Docker containers on the EC2 instances.

#### Deployment

By using `docker-compose` for deployment, we can limit the amount of time spent ssh'ing into the EC2 instances to run deploys. Furthermore, this allows other members on the Ops team to deploy application code without having direct ssh access to the EC2 instances (keeps that private key safe 🔑).

### Snapshot Volumes (`/snapper`)

Node.js was used for snapshot creation. The script does 2 things.

1. Create Snapshots for all EC2 instance volumes.
2. Deletes Snapshots that are older then 2 weeks.

#### Setup

1. [Install docker if needed](https://docs.docker.com/install/)
2. Build docker image. `docker build -t snapper .`
3. Run docker image. `docker run --env-file ./.env snapper`

#### Todo

* [ ] Convert snapshot script to Lambda function to automate snapshot process.
* [ ] Log snapshot process information to s3

### Advanced Requirements

#### General Enhancements

* [ ] Load Balance Application code between 2 EC2 instances
* [ ] Use a Docker Swarm stack instead of Docker Compose
* [ ] Convert `/snapper` snapshot tool to `AWS Lambda` function. Set to trigger on a time basis best suited for business requirements.

#### Logging Reference Architecture

With any stack, there comes failures. Logging can save essential time in debugging, enhance stack optimization and bring piece of mind to the Ops team (everyone likes to sleep at night 🌙).

There are different processes and concerns in a Ops stack worth monitoring:

* Requests made between AWS services. This includes API calls to different services such as storing snapshots in S3 buckets or monitoring IAM processes. To do this, we could use AWS Cloud Trail.
* Service load in our EC2 and RDS Instances. By monitoring service load, we can detect potentially harmful conditions such as HIGH_CPU and LOW_DISK_SPACE. To do this, we can use AWS Cloud Watch.
* Application Level logging. Consider the application code built for this test. What if every time a request was made to the app, someone received $1 off of a good they wanted. This service would surely go viral and we might want to monitor some application level metrics such as request_response_time. We could use a time based monitoring tool such as [Prometheus](https://prometheus.io/docs/introduction/overview/). We could then run our Prometheus monitoring service on AWS to keep all of our Ops tooling in the same place. Furthermore, we could export our logs from Prometheus to S3. Now we have service and application level logs in the same place 🎉!
* Another concern is what to do with these logs, how to store them, and how to best present them so that they are meaningful to the right persons. While AWS provides the ability to create metric dashboards (CloudWatch), external resource specific tooling can sometimes be helpful depending on the situation. 

#### Disaster Recovery Reference Architecture

Fortunately, AWS provides tooling for RDS DR. Similar to how EC2 Snapshots are created, we can use EBS snapshot tooling to snapshot RDS instances as well.

**First**

We need a Pub/Sub notification system in place to respond to Disaster Events. We can use `AWS SNS` to create notifications based upon database failures and initiation.

**Second**

We need to create snapshot tooling to ensure we have the latest recovery DB based upon our business requirements. We can write `AWS Lambda` functions to create snapshots of our primary DB, restore our DB with the latest snapshot in the event of failure (notified from `SNS` above), and removing old snapshots if not used.

**Third**

We can schedule our `Lambda` snapshot creation function to run on a time basis that best fits our business requirements. This function should create new snapshots, while cleaning out snapshots that are old or expired (metrics determined by business requirements).


**Summary**

AWS supplies us with the tooling necessary for DB DR. Some things not mentioned in detail here:

* Logging: Appropriate Pub/Sub logging would be necessary to notify Ops team of failure
* Costs: As with any AWS implementation, costs of tooling should be considered to best fit business requirements.
* DB Availability: When taking the initial snapshot of the DB, the snapshot process must finish before the DB returns to the "available" state again. Such bottleneck needs to be considered when designing a stack.


#### Additional Security Considerations

* [ ] Restrict API traffic to be only inbound traffic from EC2 App instances
